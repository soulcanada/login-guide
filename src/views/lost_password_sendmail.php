<h2>Recupero password.</h2>
<p>Ti abbiamo inviato una email contenente il link per recuperare la tua password.</p>
<p>Controlla ora la casella di posta.</p>
<hr/>
<ul>
	<li><strong>Modello: </strong>models/lost_password.php</li>
	<li><strong>Vista: </strong>views/lost_password_sendmail.php</li>
	<li><strong>Pagina pubblica: </strong>lost_password.php</li>
</ul>
<hr/>
<p>Questa pagina serve solamente per notificare all'utente che &egrave; stata
inviata una email all'indirizzo da lui specificato contenente un link
con cui ha la possibilit&agrave; di cambiare la propria password.</p>