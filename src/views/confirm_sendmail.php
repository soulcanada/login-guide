<h2>La registrazione &egrave; avvenuta con successo.</h2>
<p>Ti abbiamo inviato una email contenente il link per attivare il tuo account.</p>
<p>Controlla ora la casella di posta.</p>
<hr/>
<ul>
	<li><strong>Vista: </strong>views/confirm_sendmail.php</li>
	<li><strong>Pagina pubblica: </strong>confirm_sendmail.php</li>
</ul>
<hr/>
<p>Questa pagina serve solamente per notificare all'utente che &egrave; stata
inviata una email all'indirizzo da lui specificato contenente un link
con cui ha la possibilit&agrave; di attivare il proprio account.</p>