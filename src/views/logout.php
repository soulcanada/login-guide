<h1>Logout</h1>
<p>Hai eseguito correttamente il logout.</p>
<a href="login.php">Vai alla pagina di login</a>
<hr/>
<ul>
	<li><strong>Modello: </strong>models/logout.php</li>
	<li><strong>Vista: </strong>views/logout.php</li>
	<li><strong>Pagina pubblica: </strong>logout.php</li>
</ul>
<hr/>
<p>Tramite la pagina di logout un utente pu&ograve; sloggarsi dal sistema.</p>
<p>Per sloggare un utente &egrave; sufficiente eliminare le informazioni presenti nella
sessione.</p>
<p>Se un utente esegue il logout senza essersi prima loggato nel sistema, viene 
reindirizzato alla pagina di login.</p>