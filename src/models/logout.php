<?php

/*
 * Il modello della pagina di logout include
 * solamente la chiamata alla funzione sessionDestroy()
 * che elimina tutte le informazioni contenute nella sessione
 */
sessionDestroy();
