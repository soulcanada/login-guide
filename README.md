# Guida login e registrazione utenti in PHP e MySQL

L'implementazione e l'utilizzo di un sistema di **login e registrazione utenti in PHP e MySQL** è una feature
fondamentale per un sito/applicazione web e l'obiettivo di questa guida è quello di illustrarne il funzionamento
in modo semplice e chiaro.

Le lezioni sono disponibili in formato Markdown nella cartella `lessons` mentre il codice sorgente del
pacchetto di esempio è disponibile nella cartella `src`.

La documentazione online è invece disponibile all'indirizzo [http://phploginscript.altervista.org][1]

Di seguito un elenco degli argomenti che verranno trattati nella guida login e registrazione utenti in PHP e MySQL

## La pagina di login

[La pagina di login][2] è la pagina predisposta ad autenticare un utente nel sistema
cercando nel database la coppia email:password fornita dall'utente tramite il form presente nella pagina stessa;
oltre a cercare la coppia email:password nel database la pagina di login deve controllare che i dati forniti
dall'utente tramite il form siano validi altrimenti deve stampare gli eventuali errori generati.

## La pagina di registrazione utenti

[La pagina di registrazione utenti][3] è la pagina con cui un utente può iscriversi al servizio specificando
un indirizzo email valido ed una password da utilizzare poi nella pagina di login per autenticarsi nel sistema.

## La pagina di attivazione account

[La pagina di attivazione account][4] ha il compito di attivare l'account di un utente permettendogli così
di autenticarsi nel sistema; la pagina di attivazione account è raggiungibile solamente tramite il link
contenuto nell'email inviata durante la fase di registrazione utilizzando la pagina di registrazione utenti.

## La pagina di profilo utente

[La pagina di profilo utente][5] è la pagina che un utente vedrà subito dopo il login e contiene un
breve riepilogo dei dati memorizzati nel sistema; la pagina di profilo mostra come recuperare i dati di
un particolare utente utilizzando i dati memorizzati nella sessione di PHP.

## La pagina di logout

[La pagina di logout][6] è la pagina che permette ad un utente di effettuare il logout dal sistema ed il suo
funzionamento è molto semplice in quanto la principale azione che deve compiere è eliminare i dati
della sessione di PHP.

## La pagina di recupero password

[La pagina di recupero password][7] è la pagina che consente ad un utente registrato di recuperare la
password perduta inserendo il proprio indirizzo email specificato durante la registrazione.

## La pagina di cambio password

[La pagina di cambio password][8] è la pagina da cui un utente può cambiare la propria password di accesso al sistema.

[1]: http://phploginscript.altervista.org "Sfoglia la documentazione online"
[2]: 5-la-pagina-di-login.html "Leggi la pagina di login"
[3]: 6-la-pagina-di-registrazione-utenti.html "Leggi la pagina di registrazione utenti"
[4]: 7-la-pagina-di-attivazione-account.html "Leggi la pagina di attivazione account"
[5]: 8-la-pagina-di-profilo-utente.html "Leggi la pagina di profilo utente"
[6]: 9-la-pagina-di-logout.html "Leggi la pagina di logout"
[7]: 10-la-pagina-di-recupero-password.html "Leggi la pagina di recupero password"
[8]: 11-la-pagina-di-cambio-password.html "Leggi la pagina di cambio password"
[9]: 2-il-pacchetto-di-esempio.html "Leggi il pacchetto di esempio"