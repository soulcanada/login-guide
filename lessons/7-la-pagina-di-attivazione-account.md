---
title: "La pagina di attivazione account"
currentMenu: activation
---

La pagina di attivazione account ha il compito di attivare l'account di un utente permettendogli così di
autenticarsi nel sistema; la pagina di attivazione account è raggiungibile solamente tramite il link contenuto
nell'email inviata durante la fase di registrazione utilizzando la pagina di registrazione utenti.

## La pagina pubblica

La pagina pubblica la trovate nel [pacchetto di esempio][1] ed il codice è contenuto nel file `confirm.php`:
apritelo con un editor di testo per vedere il codice sorgente.

Per poter accedere alla pagina di attivazione account è necessario che venga passato in GET il
parametro token che, come detto durante l'analisi della [pagina di registrazione utenti][2], è una stringa
alfanumerica di 32 caratteri.

```php
// Se il parametro token non è stato settato
// regirigo l'utente alla&nbsp; pagina di login
if (false == isset($_GET['token']))
{
    header('Location: login.php');
}
```

Le due istruzioni successive servono per includere il modello e la vista, rispettivamente contenute in
`models/confirm.php` e `views/confirm.php`.

```php
// Includo il modello per la pagina di conferma registrazione
require_once 'models/confirm.php';

// Includo la vista per la pagina di conferma registrazione
require_once 'views/confirm.php';
```

## Il modello

Le operazioni principali che la pagina di attivazione account deve compiere sono:

1. Controllo e validazione del token ricevuto come parametro
2. Controllo esistenza token nel database
3. Attivare account dell'utente
4. Notificare all'utente l'attivazione dell'account

Per controllare la validità del token ricevuto come parametro utilizziamo la funzione `tokenIsValid()`
contenuta nel file `inc/utils.php`; per stampare eventuali errori utilizziamo invece la funzione `showFormErrors()`;

```php
// includo la lista delle funzioni di utilità
require_once 'inc/utils.php';

// Controllo se il token è valido e, se non lo è,
// stampo a video l'errore
if (false == tokenIsValid($token))
{
    $errors[] = 'Il codice specificato non è valido';
    echo showFormErrors($errors);
    exit();
}
```

Una volta che siamo sicuri che il token è valido, utilizziamo la funzione `userFindByToken()` per cercare un utente
non attivo nel database che abbia lo stesso token.

```php
// Includo la lista delle funzioni per gestire gli utenti
require_once 'inc/user.php';

// Cerco i dati di un utente in base al token
$user = userFindByToken($token);

// Se non ho trovato nessun utente genero un errore
if (false == $user)
{
    $errors[] = 'Nessun utente da attivare con il codice specificato.';
}
```

Se è stato trovato un utente non attivo nel database con il token specificato provo ad attivarne l'account.

```php
// Altrimenti se non sono riuscito ad attivare l'account dell'utente
// genero un errore
else if (false == userActivate($user['user_id']))
{
    $errors[] = 'Si è verificato un errore durante il tentativo di attivazione account';
}
```

Se durante il processo di attivazione account si sono verificati degli errori li stampo a video altrimenti il
flusso prosegue fino alla vista.

## La vista

Il codice HTML della vista lo trovate nel file `views/confirm.php`, apritelo con un editor di testo per vedere
il codice sorgente.

```php
<h1>Il tuo account è ora attivo</h1>
<p>Gentile <?php echo $userName; ?>, il tuo account è stato attivato correttamente.</p>
<a href="login.php">Esegui il login</a>
```

Notate che nella seconda riga è presente un'istruzione PHP con cui viene stampato il nome dell'utente attivato

```php
<?php echo $userName; ?>
```

Ora l'utente ha l'account attivo e può loggarsi nel sistema attraverso [la pagina di login][3].

E' il momento di passare ad analizzare [la pagina di profilo][4], pagina in cui l'utente arriva dopo aver
eseguito correttamente il login.

[1]: 2-il-pacchetto-di-esempio.html "Leggi il pacchetto di esempio"
[2]: 6-la-pagina-di-registrazione-utenti.html "Leggi la pagina di registrazione utenti"
[3]: 5-la-pagina-di-login.html "Leggi la pagina di login"
[4]: 8-la-pagina-di-profilo-utente.html "Leggi la pagina di profilo utente"
