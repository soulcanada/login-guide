---
title: "La pagina di registrazione utenti"
currentMenu: register
---

La pagina di registrazione utenti è la pagina con cui un utente può iscriversi al servizio specificando un
indirizzo email valido ed una password.

Durante la fase di registrazione è possibile richiedere all'utente tutte le informazioni che volete ma
ricordate sempre di chiedere quelle che veramente servono al vostro servizio e non informazioni superflue:
se sono troppe l'utente non si iscrive (fidatevi!).

## La pagina pubblica

La pagina pubblica della pagina di registrazione utenti la potete trovare nel file `register.php` del
[pacchetto di esempio][1].

Aprite il file con un editor di testo e guardate il codice sorgente: è lo stesso codice utilizzato
nella pagina pubblica della [pagina di login][2] quindi passiamo direttamente ad analizzare il
modello della pagina di registrazione.

## Il modello

Il modello della pagina di registrazione lo trovate in `models/register.php`: apritelo con un editor di testo per
vedere il codice sorgente.

Come per la pagina di login il modello della pagina di registrazione deve fare le seguenti cose:

1. Controllo sulla presenza e sulla validità dell'indirizzo email
2. Controllo sulla presenza e sulla validità della password
3. Controllo sulla presenza e sulla validità del nome dell'utente
4. Controllo sul database dell'eventuale esistenza dell'indirizzo email
5. Registrare il nuovo account nella tabella user
6. Inviare email contenente il link per attivare l'account sul sistema

Abbiamo visto il punto 1 ed il punto 2 durante l'analisi del modello della [pagina di login][2]
quindi passiamo direttamente a vedere come controllare e validare il nome dell'utente, indicato con il punto 3.

Il nome dell'utente è proprio il suo nome e vogliamo che abbia una lunghezza di almeno 3 caratteri;
potete cambiare il valore a vostro piacimento ed impostare un eventuale limite massimo.

```php
/* Controllo sul campo nome
 * Se la lunghezza è 0 allora il campo è vuoto
 * altrimenti controllo che il campo nome abbia una lunghezza di almeno 3 caratteri
 */
if (strlen($userName) == 0)
{
    $formErrors[] = 'Il campo nome è obbligatorio';
}
else if (strlen($userName) &lt; 3)
{
    $formErrors[] = 'Il nome inserito è troppo corto';
    }
```

Se durante la validazione dei dati sono stati riscontrati errori allora tali errori vengono stampati a
video altrimenti è possibile procedere con la registrazione di un nuovo account.

La prima cosa utile da fare è includere il file `inc/user.php` ed utilizzare la funzione `userEmailExists()`
per controllare che non sia già presente un indirizzo email uguale a quello dell'utente che sta provando a
registrarsi a sistema.

```php
// includo ora la lista di funzioni che servono per gestire l'utente
require_once 'inc/user.php';

// Per prima cosa mi assicuro che l'indirizzo email del nuovo utente
// non sia già registrato nel database
if (true == userEmailExists($userEmail))
{
    $formErrors[] = "L'indirizzo email inserito è già stato registrato";
}
```

Se l'indirizzo email non è già registrato nel sistema è possibile iniziare la procedura di registrazione che
copre i punti 5 e 6.

La password dell'utente non viene salvata in chiaro sul database ma viene criptata, creando un hash (una firma)
della password stessa: questo procedimento viene fatto usando la funzione nativa di PHP `md5()`.

```php
// La password inserita viene ora criptata tramite la funzione md5()
// criptare la password è un buon modo per alzare il livello di sicurezza
// del nostro sistema di login
$userPassword = md5($userPassword);
```

La password è diventata ora una stringa alfanumerica di 32 caratteri.

Utilizziamo lo stesso procedimento per ottenere il codice da utilizzare nel link di attivazione account
che verrà inviato al nuovo utente tramite un'email; per generare il codice utilizziamo sempre la funzione nativa di
PHP `md5()` ma, invece della password, usiamo una stringa composta dal timestamp della richiesta
(ottenibile usando la funzione nativa di PHP `time()`) e dall'indirizzo email del nuovo utente.

```php
    // La password inserita viene ora criptata tramite la funzione md5()
    // criptare la password è un buon modo per alzare il livello di sicurezza
    // del nostro sistema di login
    $userPassword = md5($userPassword);

    // Questo è il codice alfanumerico di 32 caratteri che verrà utilizzato
    // nel link di attivazione account
    $activationToken = md5(time().'_'.$userEmail);
```

Ora abbiamo tutti i dati che ci servono per registrare il nuovo utente: utilizziamo la funzione
`userRegisterNew()` contenuta nel file `inc/user.php`.

```php
/* Tento di registrare il nuovo utente sul database
 *
 * Se non riesco avverto il nuovo utente che non ho potuto registrarlo
 * altrimenti gli invio una email contenente un link con cui confermare
 * la registrazione
 */
$userData = array(
    'email'    =&gt; $userEmail,
    'password' =&gt; $userPassword,
    'name'     =&gt; $userName,
    'token'    =&gt; $activationToken
);

$userId = registerNewUser($userData);

if (false == $userId)
{
    $formErrors[] = 'Si è verificato un errore durante la registrazione';
}
```

Dopo aver registrato sul database il nuovo utente è necessario consentirgli di attivare il nuovo account:
se l'utente provasse ad autenticarsi riceverebbe un messaggio di errore.

Per fare questo, generiamo un link contenente come parametro il codice generato poco prima ed il link
finale da inviare avrà all'incirca questo aspetto:
`http://tuo.dominio/login-guide/confirm.php?token=752350e941767524ff3adbfd6c2dfddd`

```php
// Questo è il link di attivazione che serve all'utente per confermare
// la propria registrazione
$activationLink = 'http://'.$_SERVER['HTTP_HOST'];
$activationLink .= str_replace('register.php', 'confirm.php', $_SERVER['REQUEST_URI']);
$activationLink .= '?token='.$activationToken;
```

Prepariamo il messaggio di posta ed inviamolo al nuovo utente

```php
// Invio la mail in formato HTML
$headers&nbsp; = 'MIME-Version: 1.0' . "rn";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "rn";

// Oggetto e testo dell'email da inviare
$subject = 'Attivazione account';
$emailText = "<p>Gentile {$userName}, la tua registrazione è avvenuta correttamente.</p>"
           . "<p>Per attivare il tuo account, clicca sul link sottostante</p>"
           . "<p><a href="{$activationLink}">Clicca qui per attivare il tuo account</a></p><a href="{$activationLink}">";

// Provo ora ad inviare l'email all'indirizzo del nuovo utente
// Redirigo poi il nuovo utente alla pagina di conferma invio email
if (false == mail($userEmail, $subject, $emailText, $headers))
{
    $formErrors[] = "Si è verificato un errore durante il tentativo di invio dell'email di conferma";
}
else
{
    header('Location: confirm_sendmail.php');
}
```

Se l'invio è avvenuto correttamente reindirizziamo il nuovo utente ad una pagina contenente le istruzioni per
attivare il proprio account altrimenti stampiamo a video gli eventuali errori.

## La vista

La vista della pagina di registrazione utenti la trovate in `views/register.php` e contiene il codice HTML del
form da presentare al nuovo utente; questo form è composto da 5 campi di testo

* **Indirizzo email:** indirizzo email dell'utente, verrà utilizzato nella procedura di login
* **Ripeti indirizzo email:** campo in cui l'utente deve reinserire l'indirizzo email, serve come ulteriore controllo
* **Password:** password dell'utente, verrà utilizzato nella procedura di login
* **Ripeti password:** campo in cui l'utente deve reinserire la password, serve come ulteriore controllo
* **Nome:** nome da utilizzare per identificare l'utente

Ora che abbiamo analizzato la pagina di registrazione utenti è bene che iniziate ad utilizzarla:
aprite il browser e caricate la pagina **register.php** eseguendo tutta la procedura di iscrizione
fino a ricevere il messaggio email contenente il link per attivare il vostro account perchè ci servirà
durante l'analisi della [pagina di attivazione account][3].

[1]: 2-il-pacchetto-di-esempio.html "Leggi il pacchetto di esempio"
[2]: 5-la-pagina-di-login.html "Leggi la pagina di login"
[3]: 7-la-pagina-di-attivazione-account.html "Leggi la pagina di conferma attivazione account"