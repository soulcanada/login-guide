---
title: "Il pacchetto di esempio"
currentMenu: package
---

Il pacchetto di esempio allegato a questa guida contiene uno script di login e registrazione utenti che
useremo come base per studiare il funzionamento di un sistema di autenticazione; vedremo insieme quali sono le
funzionalità principali e cercheremo di spiegarne i punti più delicati.

Per provare il pacchetto di esempio della guida al login e registrazione utenti in PHP e Mysql
è necessario avere un ambiente correttamente configurato per l’esecuzione del codice PHP; leggete l’articolo
[programmare in PHP][1] per scoprire come impostare
il vostro ambiente di sviluppo e come scegliere l’IDE giusto per la vostra programmazione in PHP quotidiana.

Analizziamo ora la struttura del pacchetto di esempio contenuta nella [cartella src del progetto][2].

## La cartella inc

In questa cartella sono presenti i file con le funzioni utilizzate nello script:

* `database.php`: in questo file sono presenti le funzioni necessarie per connettersi al database
* `session.php`: in questo file sono raccolte le funzioni che permettono di utilizzare le sessioni di PHP
* `user.php`: in questo file invece abbiamo tutte le funzioni che ci permettono di gestire le operazioni sugli utenti
* `utils.php`: in questo file abbiamo le funzioni di utilità che verranno utilizzate nello script

## La cartella models

In questa cartella sono raccolti i file che contengono la logica delle nostre pagine ovvero tutte le istruzioni in PHP quali la validazione dei dati inviati dall’utente, la ricerca sul database ecc…

* `change_password.php`: contiene la logica per il funzionamento della pagina di cambio password
* `confirm.php`: contiene la logica per il funzionamento della pagina di attivazione account
* `login.php`: contiene la logica per il funzionamento della pagina di login
* `logout.php`: contiene la logica per il funzionamento della pagina di logout
* `lost_password.php`: contiene la logica per il funzionamento della pagina di recupero password
* `profile.php`: contiene la logica per il funzionamento della pagina profilo di un utente registrato
* `register.php`: contiene la logica per il funzionamento della pagina di registrazione nuovo utente

## La cartella views

In questa cartella sono raccolti i file che contengono il codice HTML utilizzato dallo script; mentre i models sanno cosa fare con i dati, le views sanno come visualizzare i dati ad un utente

* `change_password.php`: contiene il codice HTML per la pagina di cambio password
* `confirm.php`: contiene il codice HTML per la pagina di attivazione account
* `confirm_sendmail.php`: contiene il codice HTML per la pagina di conferma invio email per conferma attivazione account
* `login.php`: contiene il codice HTML per la pagina di login
* `logout.php`: contiene il codice HTML per la pagina di logout
* `lost_password.php`: contiene il codice HTML per la pagina di recupero password
* `lost_password_sendmail.php`: contiene il codice HTML per la pagina di conferma invio email per recupero password
* `profile.php`: contiene il codice HTML per la pagina di profilo utente
* `register.php`: contiene il codice HTML per la pagina di registrazione nuovo utente

Oltre alle cartelle sopra citate sono presenti anche i seguenti file:

* `change_password.php`: questa è la pagina in cui un utente può cambiare la propria password
* `confirm.php`: questa è la pagina in cui un utente può confermare la propria registrazione al sistema
* `confirm_sendmail.php`: questa è la pagina di conferma invio email per conferma registrazione account
* `login.php`: questa è la pagina in cui un utente può effettuare il login al sistema
* `logout.php`: questa è la pagina con cui un utente può sloggarsi dal sistema
* `lost_password.php`: questa è la pagina con cui un utente può richiedere il recupero della propria password
* `profile.php`: questa è la pagina che mostra ad un utente i suoi dati salvati nel sistema
* `register.php`: questa è la pagina in cui un utente può registrarsi al sistema

Come potete notare, il pacchetto di esempio è realizzato dividendo la parte di presentazione dei dati
(il codice HTML) dalla parte di elaborazione (il codice PHP) avendo così una divisione
logica che permette di ottenere un codice migliore e facilmente manutenibile;
aprendo ogni file poi noterete che contiene commenti esaustivi in ogni singolo passaggio per facilitarne la comprensione.

Nelle pagine successive analizzeremo ogni singolo file che compone l’esempio ma, prima, è necessario
[preparare il database][3] in cui salvare le informazioni di login degli utenti.

[1]: http://soulcanada.altervista.org/blog/guide/programmare-in-php "Iniziare a programmare in PHP"
[2]: https://bitbucket.org/soulcanada/login-guide/src/1d170742b26cab2aefb2df68aadd45573a4519ef/src/?at=master "Cartella src del progetto"
[3]: 3-preparare-il-database.html "Preparare il database"