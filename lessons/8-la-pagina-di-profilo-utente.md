---
title: "La pagina di profilo utente"
currentMenu: profile
---

La pagina di profilo utente è la pagina che un utente vedrà subito dopo il login e contiene un breve
riepilogo dei dati memorizzati nel sistema; la pagina di profilo mostra come recuperare i dati di un
particolare utente utilizzando i dati memorizzati nella sessione di PHP

## La pagina pubblica

Con un editor di testo aprite il file `profile.php` contenuto nel [pacchetto di esempio][1] e cominciamo ad analizzarlo.

Un utente può accedere alla pagina di profilo solamente se è loggato nel sistema altrimenti viene reindirizzato alla
pagina di login.

```php
// includo le funzioni per gestire le sessioni
require_once 'inc/session.php';

// faccio partire la sessione
sessionStart();

// Controllo se l'utente è loggato e, nel caso
// non sia loggato, lo rimando alla pagina di login
if (false == sessionUserIsLogged())
{
    header('Location: login.php');
}
```

Alla fine vengono inclusi il modello e la vista della pagina di profilo utente, collocate rispettivamente in
`models/profile.php` e `views/profile.php`.

```php
// includo il modello per la pagina del profilo
require_once 'models/profile.php';

// includo la vista per la pagina del profilo
require_once 'views/profile.php';
```

## Il modello

Il modello della pagina di profilo utente è molto semplice e le operazioni che deve fare sono:

1. Recuperare le informazioni dell'utente dalla sessione di PHP

Per poter recuperare le informazioni dell'utente dalla sessione di PHP utilizziamo la funzione
`sessionGetInformation()` che si trova nel file `inc/utils.php`.

```php
/*
 * Utilizzando la sessione, recupero le
 * informazioni di base dell'utente e,
 * tramite la vista, le stampo a video
 */
$userId    = sessionGetInformation('user_id');
$userEmail = sessionGetInformation('email');
$userName  = sessionGetInformation('name');
```

## La vista

La vista della pagina di profilo utente contiene un semplice listato dei dati dell'utente.

Nella vista della pagina di profilo potete inserire tutte le informazioni che volete, recuperandole dalla
sessione di PHP oppure dal database tramite le funzioni `userFindBy*`, contenute nel file `inc/user.php`.

Ora che il nostro utente si è loggato è giusto permettergli di effettuare un logout dal sistema e per
fare questo è presente [la pagina di logout][2].

[1]: 2-il-pacchetto-di-esempio.html "Leggi il pacchetto di esempio"
[2]: 9-la-pagina-di-logout.html "Leggi la pagina di logout"
