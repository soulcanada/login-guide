---
title: "La pagina di recupero password"
currentMenu: recover_password
---

La pagina di recupero password è la pagina che consente ad un utente registrato di recuperare la
password perduta inserendo il proprio indirizzo email specificato durante la registrazione.

Il metodo di recupero password che adotteremo consiste nell'inviare all'utente una email contenente un
link che rimanda ad una pagina che permette di impostare una nuova password, sovrascrivendo la password precedente.

## La pagina pubblica

La pagina pubblica della pagina di recupero password la trovate nel file `lost_password.php`: apritelo con
un editor di testo per vedere il codice sorgente.

La prima istruzione eseguita dalla pagina pubblica è il controllo sull'esistenza di parametri inviati
tramite POST che identifica l'invio del form di recupero password da parte dell'utente e la
successiva inclusione del modello della pagina di recupero password.

```php
// Controllo se sono presenti dei dati inviati in POST;
// se ci sono, il form è stato inviato dall'utente
if (count($_POST) &gt; 0)
{
    // Includo il modello per la pagina di recupero password
    require_once 'models/lost_password.php';
}
```

La fase successiva è il controllo sull'esistenza del parametro sendmail, che serve a visualizzare
all'utente la conferma dell'invio dell'email contenente il link per effettuare il recupero della password.

```php
// Se è presente il parametro sendmail allora l'email
// con il link di recupero è stata inviata
if (isset($_GET['sendmail']) &amp;&amp; $_GET['sendmail'] == true)
{
    require_once 'views/lost_password_sendmail.php';
}
// Altrimenti mostro il form per richiedere il recupero della password
else
{
    require_once 'views/lost_password.php';
}
```

Come potete vedere, se è presente il parametro sendmail viene inclusa una vista altrimenti ne viene inclusa un'altra.

## Il modello

Il modello della pagina di recupero password lo trovate in `models/lost_password.php` e le operazioni che deve fare
sono le seguenti:

1. Controllo sulla presenza e sulla validità dell'indirizzo email
2. Controllo sul database dell'esistenza dell'indirizzo email
3. Settare un nuovo token da utilizzare nel link di cambio password da inviare via mail all'utente
4. Inviare email con link di cambio password all'indirizzo dell'utente

Per effettuare un controllo di validità sull'indirizzo email utilizziamo la funzione `emailIsValid()`
contenuta nel file `inc/utils.php` mentre per cercare sul database l'esistenza di un utente registrato con quel
particolare indirizzo email usiamo la funzione `userEmailExists()` contenuta nel file `inc/user.php`.

```php
// includo il file con la lista delle funzioni di utilità
require_once 'inc/utils.php';

// includo ora la lista di funzioni che servono per gestire l'utente
require_once 'inc/user.php';

// Qui inseriremo gli errori avvenuti durante la validazione
// dei dati inseriti dall'utente nel form di login
$formErrors = array();

// Questi sono i dati inviati dall'utente
$userEmail = $_POST['user-email'];

// Controllo sull'indirizzo email
// Se la lunghezza è 0 allora il campo è vuoto
if (strlen($userEmail) == 0)
{
    $formErrors[] = 'Il campo email è obbligatorio.';
}
// Altrimenti controllo che l'indirizzo email sia valido
else if (false == emailIsValid($userEmail))
{
    $formErrors[] = "L'indirizzo email inserito non è corretto";
}
// altrimenti controllo che l'indirizzo email sia
// registrato al servizio
else if (false == userEmailExists($userEmail))
{
    $formErrors[] = "L'indirizzo email specificato non è registrato al servizio";
}
```

Ora che abbiamo analizzato l'indirizzo email specificato dall'utente possiamo procedere alla generazione
del token che inseriremo nel link che invieremo all'utente tramite un messaggio email.

Come detto anche nella [pagina di registrazione][1], il token è una stringa alfanumerica di 32 caratteri
ottenuta tramite l'utilizzo della funzione nativa di PHP `md5()`.

```php
// Cerco i dati dell'utente in base all'indirizzo email specificato
$user = userFindByEmail($userEmail);

// Questo token viene inserito come parametro nel link di attivazione
$token = md5(time().'_'.$user['user_id']);
```

Ottenuto il token, cerco di salvarlo nel database in corrispondenza dell'utente associato all'indirizzo email
specificato e, per fare questo, uso la funzione `userSetToken()`, contenuta nel file `inc/user.php`.

```php
// Cerco ora di registrare il token che ho generato inserendolo nei
// dati dell'utente che ha richiesto il recupero della password
if (false == userSetToken($token, $user['user_id']))
{
    $errors[] = "Si è verificato un errore durante il tentativo di recupero password.";
}
```

Settato il token all'utente desiderato è il momento di inviare una email all'utente, contenente il link
da utilizzare per accedere alla pagina di cambio password.

```php
// Questo è il link di attivazione che serve all'utente per cambiare
// la propria password
$activationLink = 'http://'.$_SERVER['HTTP_HOST'];
$activationLink .= str_replace('lost_password.php', 'change_password.php', $_SERVER['REQUEST_URI']);;
$activationLink .= '?token='.$token;

// Invio la mail in formato HTML
$headers&nbsp; = 'MIME-Version: 1.0' . "rn";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "rn";

// Oggetto e testo dell'email da inviare
$subject = 'Recupero password';
$emailText = "<p>Gentile {$user['name']},</p>"
           . "<p>per recuperare la tua password, clicca sul link sottostante</p>"
           . "<p><a href="{$activationLink}">Clicca qui per recuperare la password</a></p><a href="{$activationLink}">";

// Provo ora ad inviare l'email all'indirizzo email specificato
// Redirigo poi il nuovo utente alla pagina di conferma invio email
if (false == mail($userEmail, $subject, $emailText, $headers))
{
    $formErrors[] = "Si è verificato un errore durante il tentativo di invio dell'email di conferma";
}
else
{
    header('Location: lost_password.php?sendmail=true');
}
```

Come potete vedere dal listato sopra, una volta inviato il messaggio email l'utente viene reindirizzato
ancora alla pagina di recupero password ma questa volta includiamo anche il parametro sendmail,
il quale farà in modo che la pagina di recupero password mostri la vista contenente il codice HTML che
notifica all'utente l'invio dell'email contenente il link di cambio password.

## La vista

La pagina di recupero password ha due viste associate, rispettivamente `views/lost_password.php` e
`views/lost_password_sendmail.php`.

La vista contenuta nel file `views/lost_password.php` contiene il form che l'utente deve compilare per avviare la
procedura di recupero password mentre la vista contenuta nel file `views/lost_password_sendmail.php` contiene
un avviso da mostrare solamente quando è stata inviata l'email con il link per accedere alla pagina di cambio password.

L'utilizzo di due viste invece di una sola è stata scelta perchè risulta molto più semplice gestire
due viste distinte invece di una sola (parlando nell'ambito del nostro pacchetto di esempio).

Provate voi la procedura di recupero password fino ad ottenere l'invio dell'email di recupero
password perchè ne avremo bisogno durante l'analisi della [pagina di cambio password][2].

[1]: 6-la-pagina-di-registrazione-utenti.html "Leggi la pagina di registrazione utenti"
[2]: 11-la-pagina-di-cambio-password.html "Leggi la pagina di cambio password"