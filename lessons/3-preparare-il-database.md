---
title: "Preparare il database"
currentMenu: database
---

Per funzionare correttamente un sistema di login e registrazione utenti ha bisogno di un "posto" in cui
memorizzare le informazioni degli utenti per poterle leggere, modificare e cancellare.

Questo "posto" è la base di dati e può essere un file di testo, un file xml, un database e tanto altro ancora;
nel nostro esempio usiamo un database funzionante in MySQL per memorizzare le informazioni degli iscritti perchè
ci permette di leggerle, modificarle ed eliminarle in modo molto semplice e con pochissime righe di codice.

Potete vedere un database come un grande armadio al cui interno sono presenti dei cassetti (le tabelle) e
dentro ogni cassetto vi sono le informazioni dei nostri utenti, divise in righe:
ogni riga corrisponde ad un solo utente.

## La tabella User

Per memorizzare i dati degli utenti useremo una tabella chiamata 'user' composta dai seguenti campi:

| Nome campo | Tipo          | Indice          |
| ---------- | ------------- | --------------- |
| user_id    | INTEGER (10)  | Chiave primaria |
| email      | VARCHAR (100) | Chiave unica    |
| password   | CHAR (32)     |                 |
| name       | VARCHAR (50)  |                 |
| token      | CHAR (32)     | Indice          |
| active     | INT (1)       |                 |

Analizziamo brevemente i campi che compongono la tabella User:

* **user_id:** questo campo identifica univocamente un utente nel sistema; questo campo è la chiave primaria della tabella
* **email:** questo campo contiene l'indirizzo email di un utente; è una chiave unica ovvero non può esistere più di uno stesso indirizzo email uguale nella tabella
* **password:** questo campo contiene la password di un utente; la password viene criptata generando una stringa alfanumerica di 32 caratteri
* **name:** questo campo identifica il nome di un utente; è inserito a solo scopo illustrativo
* **token:** questo campo contiene una stringa alfanumerica di 32 caratteri utilizzata per riconoscere un utente durante l'attivazione dell'account o il recupero password
* **active:** questo campo identifica se un utente è attivo nel sistema (0 = non attivo, 1 = attivo)

## I parametri di connessione

Prima di continuare è necessario fornire a PHP i dati utili per la connessione al database:
per fare questo aprite il file `inc/database.php` e cercate queste righe di codice:

```php
// Nome del computer su cui è installato il database
$databaseHostName = 'localhost';

// Nome dell'utente che può agire sul database
$databaseUserName = 'root';

// Password dell'utente che può agire sul database
$databasePassword = '';
```

Cerchiamo di capire a cosa servono i tre parametri sopra indicati:

* `$databaseHostName`: identifica il computer su cui è installato il database; può essere un indirizzo IP oppure una stringa di testo. Per le installazioni in locale questo parametro è generalmente "localhost"
* `$databaseUserName`: identifica il nome dell'utente abilitato alla connessione e alla manipolazione di uno o più database;
* `$databasePassword`: identifica la password associata a $databaseUserName

Modificate ora i parametri impostando il valore corretto secondo le vostre impostazioni.

## Creare il database e la tabella

Nel [pacchetto di esempio][1] è presente il file `install.php` che si occupa di creare il database e la
tabella da usare nello script: apritelo per vedere il codice sorgente.

La prima cosa che facciamo è importare il file con le funzioni utili ad utilizzare il database.

```php
// Includo la lista delle funzioni per operare sul database
require_once 'inc/database.php';
```

Il passo successivo è quello di stabilire una connessione con il database in modo che PHP vi possa dialogare correttamente.

```php
// Apro una connessione con il database
$connection = openConnection();
```

Con questa istruzione stiamo creando un database di nome _'login_guide'_ che conterrà la tabella usata nel nostro esempio.

```php
// Query per la creazione del database
$sql = "CREATE DATABASE `login_guide` CHARACTER SET utf8 COLLATE utf8_unicode_ci";

// Se la creazione del database non riesce, stampo l'errore
if (false == mysql_query($sql, $connection))
{
    die("Si è verificato un errore durante la creazione del database");
}
```

Una volta creato il database creiamo la tabella 'user' che conterrà le informazioni degli utenti registrati a sistema

```php
// Query per la creazione della tabella user
$sql = "CREATE TABLE `login_guide`.`user` (
    `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    `password` char(32) COLLATE utf8_unicode_ci NOT NULL,
    `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    `token` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
    `active` int(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`user_id`),
    UNIQUE KEY (`email`),
    KEY `token` (`token`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";

// Se la creazione della tabella genera un errore, lo stampo a video
if (false == mysql_query($sql, $connection))
{
    die("Si è verificato un errore durante la creazione della tabella user");
}
```

Ora che avete tutto il necessario è tempo di installare il database e la tabella per il nostro pacchetto di esempio:
aprite il vostro browser e lanciate il file install.php; se tutto è andato per il verso giusto
dovreste ricevere a video la scritta: "_Installazione database avvenuta correttamente._"

Passiamo ora ad analizzare il [funzionamento e la struttura delle pagine][2] che compongono il pacchetto di esempio.

[1]: 2-il-pacchetto-di-esempio.html "Il pacchetto di esempio"
[2]: 4-funzionamento-struttura-delle-pagine.html "Leggi la pagina funzionamento e struttura delle pagine"
