---
title: "Introduzione alla guida"
currentMenu: index
---

L’autenticazione e la registrazione di nuovi utenti è oramai un componente essenziale per ogni sito internet
che vuole permettere ai propri visitatori di compiere azioni quali scrivere un commento, pubblicare un articolo,
acquistare un prodotto e tanto altro ancora.

L’utilizzo e la realizzazione di un sistema di autenticazione e registrazione utenti è inoltre uno degli argomenti
maggiormente discussi dai nuovi sviluppatori ed in questa guida proveremo a spiegarne il funzionamento
attraverso lo studio di un esempio pratico, analizzandone la struttura e le principali funzionalità.

## Requisiti

Prima di procedere con la lettura di questa guida è necessario avere conoscenze di base su PHP 5, MySQL e HTML;
di seguito alcune risorse che vi consiglio di leggere:

* [Introduzione alla programmazione in PHP][1]
* [Guida HTML di base][2]
* [Guida PHP di base][3]
* [Guida MySQL di base][4]

Per poter invece provare il pacchetto di esempio allegato alla guida è necessario che il vostro computer
sia correttamente configurato per poter interpretare i file PHP:

* [Guida PHP su Windows][5]
* [Guida PHP su Linux][6]

## Download del codice sorgente

Il codice sorgente del pacchetto di esempio ed i file markdown degli articoli della guida sono disponibili sul
[repository ufficiale del progetto][7] consentendovi di partecipare facilmente nello sviluppo delle future versioni della guida.

Per scaricare l'ultima versione della guida, disponibile in formato compresso, è sufficiente recarsi sulla pagina
di [download del repository][8] e cliccare sul link _Download repository_.

Bene, dopo questa piccola introduzione possiamo passare alla lezione successiva dove analizzeremo
il [pacchetto di esempio][9].

[1]: http://soulcanada.altervista.org/blog/guide/programmare-in-php "Iniziare a programmare in PHP"
[2]: http://www.html.it/guide/guida-html/ "Guida HTML"
[3]: http://www.html.it/guide/guida-php-di-base/ "Guida PHP di base"
[4]: http://www.html.it/guide/guida-mysql/ "Guida MySQL"
[5]: http://php.html.it/guide/leggi/94/guida-php-su-windows/ "Guida PHP su Windows"
[6]: http://php.html.it/guide/leggi/92/guida-php-su-linux/ "Guida PHP su Linux"
[7]: https://bitbucket.org/soulcanada/login-guide "Repository guida login e registrazione utenti in PHP e MySQL"
[8]: https://bitbucket.org/soulcanada/login-guide/downloads "Pagina di download del codice sorgente"
[9]: 2-il-pacchetto-di-esempio.html "Il pacchetto di esempio"