---
title: "La pagina di logout"
currentMenu: logout
---

La pagina di logout è la pagina che permette ad un utente di effettuare il logout dal sistema ed
il suo funzionamento è molto semplice in quanto la principale azione che deve compiere è eliminare i dati
della sessione di PHP.

L'eliminazione dei dati della sessione (e della sessione in sè) avviene utilizzando la funzione nativa di
PHP `session_destroy()`.

## La pagina pubblica

La pagina di logout funziona solamente se l'utente che l'ha richiamata è loggato nel sistema altrimenti
l'utente viene reindirizzato alla pagina di login.

```php
// includo le funzioni per gestire le sessioni
require_once 'inc/session.php';

// Faccio partire la sessione
sessionStart();

// Controllo se l'utente è loggato e,
// nel caso NON lo sia, lo rimando alla pagina di login
if (false == sessionUserIsLogged())
{
    header('Location: login.php');
}
```

Fase successiva è quella di includere il modello e la vista per la pagina di logout.

```php
// includo il modello per la pagina di logout
require_once 'models/logout.php';

// includo la vista per la pagina di logout
require_once 'views/logout.php';
```

## Il modello

Il modello della pagina di logout lo trovate nel file `models/logout.php`: apritelo con un editor di testo per
vedere il codice sorgente.

Come abbiamo detto prima l'azione che deve compiere la pagina di logout è distruggere i dati della
sessione di PHP utilizzando la funzione `sessionDestroy()`, contenuta nel file `inc/session.php`,
che internamente esegue la funzione nativa di PHP `session_destroy()`.

```php
/*
 * Il modello della pagina di logout include
 * solamente la chiamata alla funzione sessionDestroy()
 * che elimina tutte le informazioni contenute nella sessione
 */
sessionDestroy();
```

## La vista

La vista della pagina di logout contiene codice HTML utile ad informare l'utente dell'avvenuto logout dal
sistema e potete vedere il codice sorgente aprendo il file `views/logout.php` con un editor di testo.

Ora che abbiamo visto anche la pagina di logout è giunto il momento di permettere al nostro utente di
recuperare la password nel caso la dimenticasse e, per fare questo, useremo [la pagina di recupero password][1].

[1]: 10-la-pagina-di-recupero-password.html "Leggi la pagina di recupero password"
