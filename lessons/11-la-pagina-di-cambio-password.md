---
title: "La pagina di cambio password"
currentMenu: change_password
---

La pagina di cambio password è la pagina da cui un utente può cambiare la propria password di accesso al sistema.

La pagina di cambio password presente nel pacchetto di esempio è raggiungibile solamente tramite il link inviato
durante la procedura di recupero password iniziata da un utente.

## La pagina pubblica

Il codice sorgente della pagina pubblica lo trovate nel file `change_password.php`: apritelo con un editor di
testo per vederlo.

Come accade per [la pagina di attivazione account][1], anche la pagina di cambio password necessita
della presenza del parametro token altrimenti, se non presente, rimanda l'utente alla [pagina di recupero password][2].

```php
// Se il parametro token non è stato passato
// alla pagina in GET, redirigo l'utente alla pagina
// di recupero password
if (false == isset($_GET['token']))
{
    header('Location: lost_password.php');
}
```

IL passo successivo consiste nell'includere il modello e la vista della pagina di cambio password,
rispettivamente posizionati in `models/change_password.php` e `views/change_password.php`.

```php
// includo il modello per la pagina di cambio password
require_once 'models/change_password.php';

// includo la vista per la pagina di cambio password
require_once 'views/change_password.php';
```

## Il modello

Come detto prima il modello della pagina di cambio password lo trovate in `models/change_password.php` e le
operazioni che deve compiere sono le seguenti:

1. Controllo sulla validità del token passato come parametro
2. Controllo sulla presenza di un utente nel database con il token passato come parametro
3. Mostrare all'utente il form con cui cambiare la propria password
4. Controllo e validazione nuova password inserita dall'utente
5. Registrazione della nuova password inserita dall'utente
6. Confermare all'utente l'avvenuto cambio di password

Se aprite il file del modello della pagina di cambio password lo troverete vuoto perchè credo
sia giusto passare dalla teoria alla pratica.

Abbiamo visto nelle pagine precedenti come un utente possa iscriversi, loggarsi, uscire dal servizio e
visualizzare la pagina di profilo e come sono strutturati i modelli per poter compiere quelle azioni;
seguendo la scaletta di operazioni sopra citate, provate voi a realizzare il modello per la pagina di cambio password.

Ecco una lista di funzioni che potete usare durante la scrittura del modello:

* Per validare un token usate la funzione `tokenIsValid()` contenuta nel file `inc/utils.php`
* Per cercare un utente nel database tramite il token, usate la funzione `userFindByToken()`,
presente nel file `inc/user.php`
* Per validare un indirizzo email usate la funzione `emailIsValid()`, contenuta nel file `inc/utils.php`

## La vista

La vista della pagina di cambio password la potete trovare nel file `views/change_password.php` e contiene un
semplice form in cui un utente deve inserire l'indirizzo email con il quale ha effettuato la registrazione.

Dato che abbiamo detto che la vista, oltre a presentare il form di cambio password deve notificare
all'utente l'avvenuto cambio della password, vi consiglio di utilizzare una seconda vista da visualizzare
solamente se esiste il parametro sendmail.

A questo punto abbiamo analizzato tutte le pagine che compongono il [pacchetto di esempio][3] e,
se avete seguito attentamente tutto, avete ormai capito come funziona un sistema di login e registrazione utenti
in PHP e MySQL.

[1]: 7-la-pagina-di-attivazione-account.html "Leggi la pagina di attivazione account"
[2]: 10-la-pagina-di-recupero-password.html "Leggi la pagina di recupero password"
[3]: 2-il-pacchetto-di-esempio.html "Leggi il pacchetto di esempio"
