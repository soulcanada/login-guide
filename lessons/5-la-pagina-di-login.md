---
title: "La pagina di login"
currentMenu: login
---

La pagina di login è la pagina predisposta ad autenticare un utente nel sistema cercando nel database
la coppia email:password fornita dall'utente tramite il form presente nella pagina stessa; oltre a cercare
la coppia email:password nel database la pagina di login deve controllare che i dati forniti dall'utente
tramite il form siano validi altrimenti deve stampare gli eventuali errori generati.

## La pagina pubblica

Prima di analizzare il modello e la vista della pagina di login cerchiamo di capire come funziona la
pagina pubblica: aprite il file `login.php` in un editor di testo per vederne il codice sorgente.

La prima operazione che compie la pagina pubblica è l'inclusione del file `inc/session.php` che contiene la
lista delle funzioni utilizzate per gestire le sessioni di PHP.

```php
// includo le funzioni per gestire le sessioni
require_once 'inc/session.php';

```

Il passo successivo è inizializzare una sessione in PHP utilizzando la funzione
`sessionStart()` contenuta nel file `inc/session.php`

```php
// faccio partire la sessione
sessionStart();
```

La funzione `sessionStart()` esegue internamente la funzione nativa di PHP `session_start()` che serve
appunto per inizializzare una nuova sessione in PHP; fatto questo viene poi controllato se
l'utente ha già eseguito il login e, nel caso, lo si reindirizza alla pagina del profilo.

Per sapere se un utente è già loggato usiamo la funzione `sessionUserIsLogged()` contenuta nel file `inc/session.php`;
la funzione `sessionUserIsLogged()` controlla se nella sessione dell'utente è presente l'indice login
impostato a true che identifica se l'utente è loggato oppure no.

```php
// Controllo se l'utente è gia loggato
// e, nel caso lo sia, lo rimando alla pagina di profilo
if (true == sessionUserIsLogged())
{
    header('Location: profile.php');
}
```

Il passo successivo consiste nel controllare che siano presenti dei dati inviati in POST (questo vuol dire che
l'utente ha inviato il form di login) controllando la lunghezza dell'array `$_POST`.

Solamente se l'array `$_POST` ha una lunghezza maggiore di zero allora viene incluso il modello.

```php
// Altrimenti controllo se sono presenti dei dati inviati in POST;
// se ci sono, il form è stato inviato dall'utente
else if (count($_POST) &gt; 0)
{
    // includo il modello per la pagina di login
    require_once 'models/login.php';
}
```

Ultimo passo da compiere è includere la vista che contiene il codice HTML del form di login

```php
// includo la vista per la pagina di login
require_once 'views/login.php';
```

## Il modello

Come abbiamo detto più volte il modello include il codice per manipolare i dati inviati dall'utente e,
nel caso della pagina di login, il codice del modello deve fare le seguenti cose:

1. Controllo sulla presenza e sulla validità dell'indirizzo email
2. Controllo sulla presenza e sulla validità della password
3. Controllo sulla presenza della coppia email:password nel database
4. Controllo se l'utente a cui corrisponde la coppia email:password è attivo
5. Impostare il login a true nella sessione di PHP
6. Stampare eventuali errori

Tutti i punti sopra indicati sono presenti nel file `models/login.php` quindi apritelo utilizzando un editor di
testo per vedere il codice sorgente.

La prima istruzione presente nel modello è l'inclusione del file `inc/utils.php` che contiene le funzioni
necessarie per validare i dati inviati con il form.

```php
// includo il file con la lista delle funzioni di utilità
require_once 'inc/utils.php';
```

Preparo poi un array vuoto in cui inserire gli eventuali errori e setto delle variabili in cui conservare
i dati inviati dal form per poterli gestire più comodamente

```php
// Qui inseriremo gli errori avvenuti durante la validazione
// dei dati inseriti dall'utente nel form di login
$formErrors = array();

// Questi sono i dati inviati dall'utente
$userEmail = $_POST['user-email'];
$userPassword = $_POST['user-password'];
```

Ora è tempo di controllare se l'indirizzo email dell'utente è un indirizzo valido: per fare questo controllo
prima la lunghezza e successivamente la composizione utilizzando la funzione `emailIsValid()`
contenuta nel file `inc/utils.php`.

```php
/* Controllo sull'indirizzo email
 * Se la lunghezza è 0 allora il campo è vuoto
 * altrimenti controllo che l'indirizzo email sia valido
 */
if (strlen($userEmail) == 0)
{
    $formErrors[] = 'Il campo email è obbligatorio.';
}
else if (false == emailIsValid($userEmail))
{
    $formErrors[] = "L'indirizzo email inserito non è corretto";
}
```

Per il controllo della password il procedimento è molto simile alla validazione dell'indirizzo email.

Effettuati i necessari controlli di validazione sui dati e, se non sono stati riscontrati errori,
procediamo al login vero e proprio del nostro utente.

La prima cosa che facciamo è convertire la password in una stringa di 32 caratteri tramite l'uso della
funzione `md5()` di PHP; le password che salveremo sul database saranno tutte criptate per garantire un
miglior livello di sicurezza.

```php
// La password inserita viene ora criptata tramite la funzione md5()
// criptare la password è un buon modo per alzare il livello di sicurezza
// del nostro sistema di login
$userPassword = md5($userPassword);
```

Includiamo poi il file `inc/user.php` che contiene le funzioni utili a gestire gli utenti.

```php
// includo ora la lista di funzioni che servono per gestire l'utente
require_once 'inc/user.php';
```

Ora che abbiamo tutti i dati utilizziamo la funzione `authenticateUser()` per cercare la coppia email:password
nella tabella user e, se la tabella contiene la coppia email:password specificata procediamo ad autenticare
l'utente impostando alcune informazioni nella sessione PHP.

```php
/* Provo ad autenticare l'utente cercando la coppia email:password
 * nel database.
 *
 * Se riesco ad autenticarlo, lo redirigo alla sua pagina Profilo
 * inserendo nella sessione le informazioni basilari dell'utente
 */
$userId = authenticateUser($userEmail, $userPassword);
if (false == $userId)
{
    $formErrors[] = 'La coppia email/password non è corretta';
}
else
{
    // Recupero le informazioni dell'utente
    $user = userFindById($userId);

    // Aggiungo le informazioni dell'utente alla sessione
    // ed imposto il login a true, per identificare
    // che l'utente si è loggato correttamente
    sessionAddInformation('login', true);
    sessionAddInformation('user_id', $user['user_id']);
    sessionAddInformation('email', $user['email']);
    sessionAddInformation('name', $user['name']);

    // Rimando poi l'utente alla pagina del profilo
    header('Location: profile.php');
}
```

Per recuperare tutte le informazioni di un utente viene utilizzata la funzione `userFindById()`,
definita sempre nel file `inc/user.php` mentre per aggiungere informazioni alla sessione di PHP usiamo la
funzione `sessionAddInformation()` definita nel file `inc/session.php`.

Una volta impostate tutte le informazioni nella sessione redirezioniamo l'utente alla pagina del
profilo altrimenti, tramite la funzione `showFormErrors()`, vengono stampati gli errori verificatisi.

## La vista

La vista della pagina di login la trovate in `views/login.php` e contiene il codice HTML del form da presentare all'utente.

Il form di login è molto semplice in quanto formato solamente da due campi di testo,
uno per l'indirizzo email dell'utente ed uno per inserire la password; la vista non contiene codice
PHP quindi non necessita di spiegazioni dettagliate ma vi invito comunque a guardare il codice HTML.

Passiamo ora ad analizzare la [pagina di registrazione][1] di un nuovo utente

[1]: 6-la-pagina-di-registrazione-utenti.html "Leggi la pagina di registrazione utenti"
