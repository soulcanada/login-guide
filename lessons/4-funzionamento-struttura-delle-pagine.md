---
title: "Funzionamento e struttura delle pagine"
currentMenu: structure
---

Come avete potuto vedere dal [pacchetto di esempio][1], le pagine che compongono il nostro script sono
divise in modelli e viste: i modelli contengono la logica di manipolazione dei dati mentre le viste
contengono il codice HTML utile a presentare i dati agli utenti; ora invece cerchiamo di capire come
modelli e viste possono lavorare insieme.

## Le pagine pubbliche

Prendendo sempre come esempio il nostro pacchetto, possiamo dire che le pagine pubbliche sono le pagine che
vengono visualizzate da un utente tramite il browser ed è grazie ad esse che il modello e la vista
riescono a lavorare assieme.

Le pagine pubbliche del nostro esempio sono le pagine che possiamo trovare nella cartella radice del
nostro pacchetto: aprite il file `login.php` tramite un editor di testo in modo da poterne vedere il codice sorgente.

```php
// includo le funzioni per gestire le sessioni
require_once 'inc/session.php';

// faccio partire la sessione
sessionStart();

// Controllo se l'utente è gia loggato
// e, nel caso lo sia, lo rimando alla pagina di profilo
if (true == sessionUserIsLogged())
{
    header('Location: profile.php');
}
// Altrimenti controllo se sono presenti dei dati inviati in POST;
// se ci sono, il form è stato inviato dall'utente
else if (count($_POST) --> 0)
{
    // includo il modello per la pagina di login
    require_once 'models/login.php';
}

// includo la vista per la pagina di login
require_once 'views/login.php';
```

Soffermiamoci un momento su questo pezzo di codice

```php
// Altrimenti controllo se sono presenti dei dati inviati in POST;
// se ci sono, il form è stato inviato dall'utente
else if (count($_POST) &gt; 0)
{
    // includo il modello per la pagina di login
    require_once 'models/login.php';
}
// includo la vista per la pagina di login
require_once 'views/login.php';
```

Notate che il modello della pagina viene incluso solamente se l'utente ha inviato il form di
login presente nella vista mentre la vista viene sempre inclusa e mostrata all'utente.

Le pagine pubbliche contengono a loro volta codice PHP con cui è possibile comandare il comportamento
della pagina senza intaccare il codice HTML della vista od il codice PHP del modello.

Dopo questa breve panoramica possiamo entrare nel vivo della guida iniziando ad analizzare il
[funzionamento della pagina di login][2].

[1]: 2-il-pacchetto-di-esempio.html "Leggi la pagina il pacchetto di esempio"
[2]: 5-la-pagina-di-login.html "Leggi la pagina di login"
