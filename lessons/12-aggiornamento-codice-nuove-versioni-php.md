---
title: "Aggiorniamo il codice alle nuove versioni di PHP"
currentMenu: update_php_version
---
Essendo passati ormai 3 anni dalla prima pubblicazione della
[guida al login e registrazione utenti in PHP e MySQL][1], credo sia arrivato il momento di
aggiornare il codice del [pacchetto di esempio][2] rimuovendo le funzioni ormai deprecate.

Quando una funzione viene deprecata vuol dire che verrà rimossa nelle future versioni di PHP e deve quindi essere
sostituita con una controparte aggiornata; per nostra fortuna l'unico intervento che dobbiamo fare
è sostituire le chiamate alle funzioni `mysql_*` con le funzioni della libreria [MySQLI][3]: `mysqli_*`.

Le modifiche si concentreranno sul file `inc/database`, che contiene le funzioni di collegamento al database
ed il file `inc/user.php`, che contiene le funzioni operanti sulla tabella degli utenti.

## Aggiorniamo il file database.php

Aprite il file `inc/database.php` e sostituitene il contenuto con quello sottostante

```php
<?php

// Qui salviamo il link di connessione al database
static $connectionLink = null;

/*
 * Il questo file sono contenute le funzioni che servono
 * per dialogare con il database
 */

// Questa funzione serve per ottenere la connessione al database
function getConnection()
{
    // Qui è memorizzato il link alla connessione
    global $connectionLink;

    // Se il link alla connessione è NULL
    if (true == is_null($connectionLink))
    {
        // Imposto un link di connessione al database
        $connectionLink = createConnection();
    }

    // Ritorno il link alla connessione
    return $connectionLink;
}

// Questa funzione crea una connessione tra PHP ed il database
function createConnection()
{
    // Nome del computer su cui è installato il database
    $databaseHostName	= 'localhost';

    // Nome dell'utente che può agire sul database
    $databaseUserName 	= 'root';

    // Password dell'utente che può agire sul database
    $databasePassword 	= '';

    // Questo è il nome del database, modificalo
    // in base alle tue esigenze
    $databaseName = 'login_guide';

    // Provo a stabilire una connessione
    $connection = mysqli_connect($databaseHostName, $databaseUserName, $databasePassword, $databaseName);

    // Se la connessione non è riuscita
    // stampo un errore
    if (false == $connection)
    {
        die("Si &egrave; verificato un errore durante la connessione al database. Ricontrolla i dati di accesso");
    }

    // nessun errore, ritorno il link alla connessione
    return $connection;
}
```

Rispetto alla precedente versione del file, sono state sostituite le chiamate alle funzioni `mysql_*` con la
controparte `mysqli_*`, snellendo il codice perchè la selezione del database viene specificata direttamente
nella chiamata alla funzione `mysqli_connect`.

Per rendere il codice più leggibile la funzione `openConnection` è stata rinominata in `createConnection` e
spostata in basso perchè chiamata dopo la funzione `getConnection`.

Arrivati a questo punto viene spontaneo notare come i commenti risultino ridondanti il quanto il codice
stesso può essere utilizzato direttamente come documentazione del suo stesso funzionamento; rimuovendo
tutti i commenti dovreste avere ora il nuovo file pronto all'uso.

```php
<?php

static $connectionLink = null;

function getConnection()
{
    global $connectionLink;

    if (true == is_null($connectionLink))
    {
        $connectionLink = createConnection();
    }

    return $connectionLink;
}

function createConnection()
{
    $databaseHostName	= 'localhost';
    $databaseUserName 	= 'root';
    $databasePassword 	= '';
    $databaseName = 'login_guide';

    $connection = mysqli_connect($databaseHostName, $databaseUserName, $databasePassword, $databaseName);

    if (false == $connection)
    {
        die("Si è verificato un errore durante la connessione al database. Ricontrolla i dati di accesso");
    }

    return $connection;
}
```

## Aggiorniamo il file user.php

Aprite ora il file `inc/user.php` e sostituite il suo contenuto con quello sottostante.

```php
<?php

/*
 * In questo file sono contenute le funzioni utili
 * alla gestione degli utenti
 */

// Includo la lista delle funzioni per dialogare con il database
require_once 'database.php';

// Questa funzione si occupa di autenticare un utente
// nel sistema
function authenticateUser($userEmail, $userPassword)
{
	// Apro una connessione con il database
	$connection = getConnection();

	// Cerco nel database un utente attivo
	// con la coppia email:password specificata
	$sql = "SELECT user_id
			FROM user
			WHERE
				email = '%s'
			AND
				password = '%s'
			AND
				active = 1";

	// Assegno alla query i parametri da cercare
	$sql = sprintf($sql, $userEmail, $userPassword);

	// Eseguo la query sul database
	$result = mysqli_query($connection, $sql);

	// Se si è verificato un errore oppure non ho trovato nessun risultato
	if (false == $result || mysqli_num_rows($result) == 0)
		return false;

	// Questa riga contiene le informazioni dell'utente, se trovato nel database
	$row = mysqli_fetch_assoc($result);

	// Ritorno lo user_id dell'utente
	return $row['user_id'];
}

// Questa funzione permette di registrare un nuovo utente nel sistema
function registerNewUser($userData)
{
	// Apro una connessione con il database
	$connection = getConnection();

	// Questi sono i dati da inserire nel database
	$userEmail 		= $userData['email'];
	$userPassword 	= $userData['password'];
	$userName		= $userData['name'];
	$token 			= $userData['token'];

	// Query per inserire i nuovi dati nel database
	$sql = "INSERT INTO user
				(email, password, name, token)
			VALUES
				('%s', '%s', '%s', '%s') ";

	// Assegno alla query i parametri da cercare
	$sql = sprintf($sql,
				$userEmail,
				$userPassword,
				mysqli_real_escape_string($userName),
				$token);

	// Provo ad inserire i dati
	if (false == mysqli_query($connection, $sql))
	{
		return false;
	}
	// se sono riuscito ad inserire i dati,
	// ritorno l'ultimo user_id inserito
	else
	{
		return mysqli_insert_id($connection);
	}
}

// Questa funzione controlla l'esistenza
// nel database di un utente con uno specifico indirizzo email
function userEmailExists($userEmail)
{
	// Apro una connessione con il database
	$connection = getConnection();

	// Conto il numero di utenti registrati con
	// l'indirizzo email specificato
	$sql = "SELECT user_id
			FROM user
			WHERE
				email = '%s' ";

	// Assegno alla query i parametri da cercare
	$sql = sprintf($sql, $userEmail);

	// Eseguo la query sul database
	$result = mysqli_query($connection, $sql);

	// Se non ho trovato utenti oppure se si è
	// verificato un errore
	if (false == $result || mysqli_num_rows($result) == 0)
	{
		return false;
	}
	// Altrimenti vuol dire che ho trovato un utente
	// con l'indirizzo email specificato
	else
	{
		return true;
	}
}

// Questa funzione cerca i dati di un utente
// in base al token specificato
function userFindByToken($token)
{
	// Apro una connessione con il database
	$connection = getConnection();

	// Cerco un utente con un certo token
	$sql = "SELECT *
			FROM user
			WHERE
				token = '%s'";

	// Assegno alla query i parametri da cercare
	$sql = sprintf($sql, $token);

	// Eseguo la query sul database
	$result = mysqli_query($connection, $sql);

	// Se si è verificato un errore oppure non
	// ho trovato nessun utente
	if (false == $result || mysqli_num_rows($result) == 0)
	{
		return false;
	}

	// Ritorno i dati dell'utente trovato
	return mysqli_fetch_assoc($result);
}

// Questa funzione cerca i dati di un utente
// in base all'indirizzo email specificato
function userFindByEmail($userEmail)
{
	// Apro una connessione con il database
	$connection = getConnection();

	// Cerco un utente con un certo indirizzo email
	$sql = "SELECT *
			FROM user
			WHERE
				email = '%s'";

	// Assegno alla query i parametri da cercare
	$sql = sprintf($sql, $userEmail);

	// Eseguo la query sul database
	$result = mysqli_query($connection, $sql);

	// Se si è verificato un errore oppure non
	// ho trovato nessun utente
	if (false == $result || mysqli_num_rows($result) == 0)
	{
		return false;
	}

	// Ritorno i dati dell'utente trovato
	return mysqli_fetch_assoc($result);
}

// Questa funzione cerca i dati di un utente
// in base ad un userId specificato
function userFindById($userId)
{
	// Apro una connessione con il database
	$connection = getConnection();

	// Cerco un utente con un certo userId
	$sql = "SELECT *
			FROM user
			WHERE
				user_id = %d";

	// Assegno alla query i parametri da cercare
	$sql = sprintf($sql, $userId);

	// Eseguo la query sul database
	$result = mysqli_query($connection, $sql);

	// Se si è verificato un errore oppure non
	// ho trovato nessun utente
	if (false == $result || mysqli_num_rows($result) == 0)
	{
		return false;
	}

	// Ritorno i dati dell'utente trovato
	return mysqli_fetch_assoc($result);
}

// Questa funzione serve per attivare l'account
// di un utente con un certo userId
function userActivate($userId)
{
	// Apro una connessione con il database
	$connection = getConnection();

	// Attivo l'utente impostando il campo
	// active a 1
	$sql = "UPDATE user
			SET active = 1, token = NULL
			WHERE
				user_id = %d";

	// Assegno alla query i parametri da cercare
	$sql = sprintf($sql, $userId);

	// Eseguo la query sul database
	$result = mysqli_query($connection, $sql);

	// Se si è verificato un errore oppure nessun utente
	// � stato attivato
	if (false == $result || mysqli_affected_rows($connection) == 0)
	{
		return false;
	}
	// Altrimenti l'utente � stato attivato
	else
	{
		return true;
	}
}

// Questa funzione permette di settare il token
// di uno specifico utente, identificato dal suo userId
function userSetToken($token, $userId)
{
	// Apro la connessione al database
	$connection = getConnection();

	// Questa è la query di aggornamento
	$sql = "UPDATE user
			SET token = '%s'
			WHERE user_id = %d";

	// Assegno alla query i parametri da cercare
	$sql = sprintf($sql, $token, $userId);

	// Eseguo la query
	$result = mysqli_query($connection, $sql);

	// Se si è verificato un errore oppure nessun token � stato settato
	// ritorno false
	if (false == $result || mysqli_affected_rows($connection) == 0)
	{
		return false;
	}
	// altrimenti ritorno true
	else
	{
		return true;
	}
}
```

Anche in questo caso le modifiche sono state la sostituzione delle funzioni `mysql_*` con la controparte
`mysqli_*`

Per il momento non effettuaiamo nessun'altra modifica a questo file perchè, negli articoli successivi, vedremo
come rifattorizzarne il codice eliminando le parti ridondanti.

# Versioni supportate

L'aggiornamento del codice del [pacchetto di esempio][2] permette ora di supportare le versioni di PHP
a partire dalla 5.3.

[1]: intro.html "Introduzione alla guida login e registrazione utenti"
[2]: 2-il-pacchetto-di-esempio.html "Il pacchetto di esempio"
[3]: http://php.net/manual/en/book.mysqli.php "L'estensione PHP MySQLI"